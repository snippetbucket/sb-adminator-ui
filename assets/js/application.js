$(function(){

	var ApplicationRouter = Backbone.Router.extend({

		//map url routes to contained methods
		routes: {
			
			"enquiry": "enquiry"
		},
		
        enquiry: function(){
           var form = FormView();
        },

	});

	var ApplicationView = Backbone.View.extend({

		el: $('#sidebar-menu'),
		//observe navigation click events and map to contained methods
		events: {
			'click #enquiry': 'displayEnquiry'
		},

		//called on instantiation
		initialize: function(){
			//set dependency on ApplicationRouter
			this.router = new ApplicationRouter();

			//call to begin monitoring uri and route changes
			Backbone.history.start();
		},

		displayEnquiry: function(){
			//update url and pass true to execute route method
			// this.router.navigate("enquiry", true);
			$('#mainContent').append("<h1>working</h1>");
           	console.log("Great Error Message.");
		}

	});

	//load application
	new ApplicationView();

	var FormView = Backbone.View.extend({
	
	el : ".app-form"

	});


});

