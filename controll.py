import jinja2,os,base64
from jinja2 import Environment, FileSystemLoader
from japronto import Application
import os,jsonpickle
from jinja2 import Environment, DictLoader,Template
from jinja2 import Environment, PackageLoader, select_autoescape
from pymongo import MongoClient
from functools import partial
from bson.objectid import ObjectId

# env = Environment(
#     loader=PackageLoader('demo-core-sample-adminuix', 'templates'),
#     autoescape=select_autoescape(['html', 'xml'])
# )

# e = Environment(loader=DictLoader(dict(
#     layout='/var/www/html/demo-core-sample-adminuix/menu.html',
#     index='{% extends "/var/www/html/demo-core-sample-adminuix/menu.html" %}'
# )))
# template = Template("menu.html")
env = Environment(
    loader=FileSystemLoader(os.path.dirname(os.path.realpath(__file__)), 'templates'),
    autoescape=select_autoescape(['html', 'xml'])
)

templateLoader = jinja2.FileSystemLoader(searchpath=os.path.dirname(os.path.realpath(__file__))+'/templates')
templateEnv = jinja2.Environment(loader=templateLoader)




def hello(request):
	print (jsonpickle.encode(request),123)
	template = templateEnv.get_template("menu.html")
	return request.Response(text=template.render(), mime_type='text/html')
    # with open('menu.html') as html_file:
    #     return request.Response(text=html_file.read(), mime_type='text/html')


def enquiry(request):
	template = templateEnv.get_template("enquiry.html")
	return request.Response(text=template.render(), mime_type='text/html')
    # with open('enquiry.html') as html_file:
    #     return request.Response(text=html_file.read(), mime_type='text/html')

def suggession(request):
	template = templateEnv.get_template("suggessions.html")
	return request.Response(text=template.render(), mime_type='text/html')

def call(request):
	client = MongoClient('mongodb://192.168.1.146:27017/')
	db = client.test_kaushik
	collection = db.circular
	if request.form and request.form['image']:
		dir_path = os.path.dirname(os.path.realpath(__file__))
		img_file = request.form['image'].split('base64,')
		# print (img_file[1])
		
		# fh = open(dir_path+'/'+request.form['image_name'], "wb")
		# fh.write(img_file[1].encode())
		# fh.close()

		with open(dir_path+'/'+request.form['image_name'], "wb") as fh:
			fh.write(base64.decodebytes(img_file[1].encode()))
		
		print(dir_path+request.form['image_name'])
	print(len(request.form['image']))
	post_id = collection.insert_one(request.form)
	# for col in collection.find(): 
	print (collection.count())
	# collection.delete_many({})
	#print (request.form,123)
	return request.Response(text="1234")

def getweeklyreport(request):
	client = MongoClient('mongodb://192.168.1.146:27017/')
	db = client.test_kaushik
	collection = db.weeklyreport
	data =  collection.find({})
	list1 = []
	for d in data:
		d.update({'_id':str(d['_id'])})
		list1.append(d)
	print (list1)
	# data = str(jsonpickle.encode(data))
	# post_id = collection.insert_many([{'date':'13/04/2018','subject':'english','total_marks':100,'obtained_marks':85},
	# 	{'date':'14/04/2018','subject':'hindi','total_marks':100,'obtained_marks':80},
	# 	{'date':'15/04/2018','subject':'general knowledge','total_marks':100,'obtained_marks':85}])
	# for col in collection.find(): 
	# print (collection.count())
	#print (request.form,123)
	return request.Response(json=list1)

def getresult(request):
	client = MongoClient('mongodb://192.168.1.146:27017/')
	db = client.test_kaushik
	collection = db.result
	data =  collection.find({})
	list1 = []
	for d in data:
		d.update({'_id':str(d['_id'])})
		list1.append(d)
	return request.Response(json=list1)

def getcircular(request):
	client = MongoClient('mongodb://192.168.1.146:27017/')
	db = client.test_kaushik
	collection = db.circular
	print(jsonpickle.encode(db.collection_names(include_system_collections=False)))
	data =  collection.find({},{"image":0})
	list1 = []
	for d in data:
		d.update({'_id':str(d['_id'])})
		list1.append(d)
	print (list1)
	# print(list1[-1]['image'])
	return request.Response(json=list1)

def attendance(request):
	client = MongoClient('mongodb://192.168.1.146:27017/')
	db = client.test_kaushik
	collection = db.attendance
	data =  collection.find({"attendance":{'$exists': 1}})
	print(data)
	for d in data:
		data = d['attendance']
	return request.Response(json=data)

def syllabus(request):
	client = MongoClient('mongodb://192.168.1.146:27017/')
	db = client.test_kaushik
	collection = db.syllabus
	data =  collection.find({"subject":request.form['subject']})
	print(data)
	for d in data:
		d.update({'_id':str(d['_id'])})
		list1 = d
	print(data)
	return request.Response(json=list1)

def events(request):
	client = MongoClient('mongodb://192.168.1.146:27017/')
	db = client.test_kaushik
	collection = db.events
	data =  collection.find({})
	list1 = []
	print(data)
	for d in data:
		d.update({'_id':str(d['_id'])})
		list1.append(d)
	print(data)
	return request.Response(json=list1)

def paper(request):
	client = MongoClient('mongodb://192.168.1.146:27017/')
	db = client.test_kaushik
	collection = db.paper
	data =  collection.find({"paper":request.form['paper']})
	print(data)
	for d in data:
		d.update({'_id':str(d['_id'])})
		list1 = d
	print(list1)
	return request.Response(json=list1)

def homework(request):
	client = MongoClient('mongodb://192.168.1.146:27017/')
	db = client.test_kaushik
	collection = db.homework
	data =  collection.find({"homework":request.form['homework']})
	print(data)
	for d in data:
		d.update({'_id':str(d['_id'])})
		list1 = d
	print(list1)
	return request.Response(json=list1)

def edutimetable(request):
	client = MongoClient('mongodb://192.168.1.146:27017/')
	db = client.test_kaushik
	collection = db.edu_timetable
	data =  collection.find({"timetable":request.form['timetable']})
	print(data)
	for d in data:
		d.update({'_id':str(d['_id'])})
		list1 = d
	print(list1)
	return request.Response(json=list1)

def weeklytimetable(request):
	client = MongoClient('mongodb://192.168.1.146:27017/')
	db = client.test_kaushik
	collection = db.weekly_timetable
	data =  collection.find({"timetable":request.form['timetable']})
	print(data)
	for d in data:
		d.update({'_id':str(d['_id'])})
		list1 = d
	print(list1)
	return request.Response(json=list1)

def assignment(request):
	client = MongoClient('mongodb://192.168.1.146:27017/')
	db = client.test_kaushik
	collection = db.assignment
	data =  collection.find(request.form)
	print(data)
	for d in data:
		d.update({'_id':str(d['_id'])})
		list1 = d
	print(list1)
	return request.Response(json=list1)


def profile(request):
	client = MongoClient('mongodb://192.168.1.146:27017/')
	db = client.test_kaushik
	collection = db.studentrecords
	data =  collection.find({})
	print(data)
	for d in data:
		d.update({'_id':str(d['_id'])})
		list1 = d
	print(list1)
	return request.Response(json=list1)

def photos(request):
	client = MongoClient('mongodb://192.168.1.146:27017/')
	db = client.test_kaushik
	collection = db.photos
	data =  collection.find({})
	print(data)
	list1 = []
	for d in data:
		d.update({'_id':str(d['_id'])})
		list1.append(d)
	print(list1)
	return request.Response(json=list1)

def achievements(request):
	client = MongoClient('mongodb://192.168.1.146:27017/')
	db = client.test_kaushik
	collection = db.achievements
	data =  collection.find({})
	print(data)
	list1 = []
	for d in data:
		d.update({'_id':str(d['_id'])})
		list1.append(d)
	print(list1)
	return request.Response(json=list1)

def holidays(request):
	client = MongoClient('mongodb://192.168.1.146:27017/')
	db = client.test_kaushik
	collection = db.holidays
	print (request.form,"ggfg")
	# print(request.form,jsonpickle.encode(db.collection_names(include_system_collections=False)))
	data =  collection.find({"events":{'$exists': 1}})
	# list1 = []
	# print(data)
	for d in data:
		# print(d['events'],"data")
		data = d['events']
		# list1.append(d)
	print(data)
	# print (12,list1)
	# collection.delete_many({})

	# print(list1[-1]['image'])
	return request.Response(json=data)

def static(request):
    with open(request.path[1:], 'rb') as static_file:
        return request.Response(body=static_file.read())

app = Application()
# template = env.get_template('enquiry.html')
# e = Environment(loader=FileSystemLoader('templates/'))
# japronto_jinja2.setup(app, loader=jinja2.FileSystemLoader(os.path.dirname(os.path.realpath(__file__))+'/templates/'))
# template_env = Environment(loader=FileSystemLoader(os.path.dirname(os.path.realpath(__file__))+'/templates/'),trim_blocks=True)
# template_env = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(os.path.realpath(__file__))+'/templates/'))
# template_env.get_template('enquiry.html')
app.router.add_route('/call', call)
app.router.add_route('/events', events)
app.router.add_route('/photos', photos)
app.router.add_route('/getprofile', profile)
app.router.add_route('/holidays', holidays)
app.router.add_route('/syllabus', syllabus)
app.router.add_route('/paper', paper)
app.router.add_route('/achievements', achievements)
app.router.add_route('/homework', homework)
app.router.add_route('/assignment', assignment)
app.router.add_route('/edutimetable', edutimetable)
app.router.add_route('/weeklytimetable', weeklytimetable)
app.router.add_route('/attendance', attendance)
app.router.add_route('/getweeklyreport', getweeklyreport)
app.router.add_route('/getresult', getresult)
app.router.add_route('/getcircular', getcircular)
app.router.add_route('/', hello)
app.router.add_route('/enquiry', enquiry)
app.router.add_route('/suggession', suggession)

app.router.add_route('/{1}', static)
app.router.add_route('/static/{1}', static)
app.router.add_route('/static/{1}/{2}', static)
app.router.add_route('/static/{1}/{2}/{3}', static)

app.run(debug=True)
