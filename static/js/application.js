$(function(){

	var QWeb = new QWeb2.Engine();
	QWeb.add_template("static/xml/template.xml");
	var FormView = Backbone.View.extend({
	
		el : $('.app-form'),
		obj:{},
		count:0,
		model: false,
		events: {
			'click #submit1'  : 'isValidate',
			'change #my_form' : 'changeimage',
			'click #holidays' : 'changemonth',
			'click #foodcal' : 'foodcal',
			'click #syllabusget' : 'syllabus',
			'click #paperget' : 'paper',
			'click #homeworkget' : 'homework',
			'click #edutimetableget' : 'edutimetable',
			'click #weeklytimetableget' : 'weeklytimetable',
			'click #assignmentget' : 'assignment',
		},
		
		initialize: function(op){
			// self.obj = {};
			console.log(self.obj,"before",op);
			if (self.obj !== undefined){
				self.obj = undefined;
			}
			// console.log(self.obj,"after");
			this.model =  op && op.model;
			this.template =  op && op.template;
			// console.log(this.model ,this.template,op.data);
			this.el.html(QWeb.render(this.template, {'model':this.model,'data':op.data}))
			$(".app-title").text(op.title);
			var $calendar = $('#calendar');
			var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
			var date = new Date();
			if (this.model == 'holidays'){
				$.post("/holidays",{"month":months[date.getMonth()],"year":date.getFullYear()},function(a,b){
					console.log(months[date.getMonth()],date.getFullYear());
	  				// console.log("a>>>>>>>>>",a);
					$calendar.fullCalendar({
			            viewRender: function() {
			                $('#month').empty();
				            var $month = $('#month');
				        	var $calendar = $('#calendar');
				            var month = $calendar.fullCalendar('getDate');
				            var initial = month.format('YYYY-MM');
				            month.add(-12, 'month');
				            for (var i = 0; i < 18; i++) {
				                var opt = document.createElement('option');
				                opt.value = month.format('YYYY-MM-01');
				                opt.text = month.format('MMM YYYY');
				                opt.selected = initial === month.format('YYYY-MM');
				                $month.append(opt);
				                month.add(1, 'month');
				            }
			            },

			            events:a
		            
		        	});
	  			});
	  		}
	  		if (this.model == 'foodcal'){
				$calendar.fullCalendar({
		            viewRender: function() {
		                $('#month').empty();
			            var $month = $('#month');
			        	var $calendar = $('#calendar');
			            var month = $calendar.fullCalendar('getDate');
			            var initial = month.format('YYYY-MM');
			            month.add(-12, 'month');
			            for (var i = 0; i < 18; i++) {
			                var opt = document.createElement('option');
			                opt.value = month.format('YYYY-MM-01');
			                opt.text = month.format('MMM YYYY');
			                opt.selected = initial === month.format('YYYY-MM');
			                $month.append(opt);
			                month.add(1, 'month');
			            }
		            },

	        	});
	  		}
	  		if (this.model == 'attendance'){
				$.get("/attendance",function(a,b){
	  				console.log("a>>>>>>>>>",a);
					$calendar.fullCalendar({
			            viewRender: function() {
			                $('#month').empty();
				            var $month = $('#month');
				        	var $calendar = $('#calendar');
				            var month = $calendar.fullCalendar('getDate');
				            var initial = month.format('YYYY-MM');
				            month.add(-12, 'month');
				            for (var i = 0; i < 18; i++) {
				                var opt = document.createElement('option');
				                opt.value = month.format('YYYY-MM-01');
				                opt.text = month.format('MMM YYYY');
				                opt.selected = initial === month.format('YYYY-MM');
				                $month.append(opt);
				                month.add(1, 'month');
				            }
			            },

			            events:a
		            
		        	});

	  			});
	  		}
		},
		syllabus:function(e){
			var subject = {"subject":$("#subject").val()}
			$.post("/syllabus",subject,function(a,b){
				console.log(a);
  				$('.app-form').html(QWeb.render("curriculum_form", {'model':"curriculum",'data':a}))
  			});
		},
		paper:function(e){
			var paper = {"paper":$("#paper1").val()}
			$.post("/paper",paper,function(a,b){
				console.log(a);
  				$('.app-form').html(QWeb.render("curriculum_form", {'model':"curriculum",'datapaper':a}))  				
  			});
		},
		homework:function(e){
			var homework = {"homework":$("#homework").val()}
			$.post("/homework",homework,function(a,b){
				console.log(a);
  				$('.app-form').html(QWeb.render("homework_form", {'model':"homework",'data':a}))
  			});
		},
		assignment:function(e){
			var assignment = {"subject":$("#subject").val(),"month":$("#month").val()}
			console.log($("#subject").val(),$("#month").val())
			$.post("/assignment",assignment,function(a,b){
				console.log(a);
  				$('.app-form').html(QWeb.render("assignment_form", {'model':"assignment",'data':a}))
  			});
		},
		edutimetable:function(e){
			var homework = {"timetable":$("#timetable").val()}
			$.post("/edutimetable",homework,function(a,b){
				console.log(a);
  				$('.app-form').html(QWeb.render("edutimetable_form", {'model':"edutimetable",'data':a}))
  			});
		},
		weeklytimetable:function(e){
			var homework = {"timetable":$("#timetable").val()}
			$.post("/weeklytimetable",homework,function(a,b){
				console.log(a);
  				$('.app-form').html(QWeb.render("weeklytimetable_form", {'model':"weeklytimetable",'data':a}))
  			});
		},
		foodcal:function(e){
			var $calendar = $('#calendar'),year = $('#month').val().split('-')[0];
			$calendar.fullCalendar('gotoDate', $('#month').val());
		},
		changemonth:function(e){
			var $calendar = $('#calendar'),year = $('#month').val().split('-')[0];
			$calendar.fullCalendar('gotoDate', $('#month').val());
			console.log($('#month').val());
			dict = {'year':year,'month':$('#month').val().split('-')[1]}
			$.post("/holidays",dict,function(a,b){
  				console.log("a>>>>>>>>>",a.title);
  			});
  			// var dict = {};
			// for (var i,j in a.title,a.events){
			// 		dict['title'] = a.title;
			// 		dict['start'] = a.events;
			// }
  			

		},
		changeimage: function(e){
			self.obj = {};
			var ip = this.$('.input'),txtarea = this.$('.textarea');
			var checkbox = this.$('.checkbox1'),radio = this.$('.radio1');
  			var select = this.$('.select'),image = $('.image');
  			var list = [ip,txtarea,select];
  			var list1 = [radio,checkbox];
			var obj = {} , image = $('.image');
			
			// TextArea , Input , Select Function For More Than One
  			_.each(list,function(data){
  			 	_.each(data,function(num){
	  			 	var num1 = $('#'+num.id).val();
	  			 	obj[num.id] = num1;
  				});
  			});

			// Radio , Checkbox Function For More Than One
  			_.each(list1,function(data){
  			 	_.each(data,function(num){
	  			 	var num1 = $('#'+num.id).attr("checked") ? 'True' : 'False';
	  			 	obj[num.id] = num1;
  				});
  			});

			$.when(
			// Image Function For More than one
			_.each(image,function(data){
				var image = data.files[0];
  				if (image !== undefined){
  					console.log(image.name,123);
	  				var reader = new FileReader();
			   		reader.onloadend = function (e) {
			   			// console.log(reader.result);
			   			// console.log(e.target.result,8888);
			   			// $('#image').attr('src', e.target.result);
			   			// storeTheImage(); 
			   			obj['image_name'] = 'static/images/circular/'+image.name
			   			obj[data.id] = reader.result;
			   			
					},
			   		reader.readAsDataURL(image);
				} else {
					obj[data.id] = '';
				}
			})
			).then(
			setTimeout(function(){
				self.obj = obj;
			},10));

		},
		isValidate: function(){
			var ip = this.$('.input'),txtarea = this.$('.textarea');
			console.log(self.obj,"22334");
			var checkbox = this.$('.checkbox1'),radio = this.$('.radio1');
  			var select = this.$('.select'),image = $('.image');
  			var list = [ip,txtarea,select];
  			var list1 = [radio,checkbox];
			var obj = {} , image = $('.image');
			var val1 = '';
			_.each(image,function(data){
				_.each(data,function(num){
					obj[num.id] = '';
				});
			});
			_.each(list,function(data){
  			 	_.each(data,function(num){
	  			 	obj[num.id] = '';
  				});
  			});
			_.each(list1,function(data){
  			 	_.each(data,function(num){
	  			 	obj[num.id] = '';
  				});
  			});
  			if (self.obj === undefined){
  				self.obj = obj;
  			}
  			_.each(self.obj,function(value,key){
				var li = document.getElementById(key);
				if (li !== null){
					var cl = li.className.split(" ");
				}
				if (_.contains(cl, 'require')){
					if (value == ''){
						// self.obj = {};
						$('#error').text("Please Add All required Fields...");
						$('#myModal').css("display", "block");
						val1 = 'yes';
						console.log("Please Add All required Fields...");
					}
				}

			});
			if (val1 == ''){
				this.submit();
			}

		},
		submit: function(e){
			// event.stopImmediatePropagation();	
			// console.log(self.obj,"1222");
			$.post("/call",self.obj,function(a,b){
  				console.log("a>>>>>>>>>",a);
  			});
		},
		buildMonthList:function() {
            $('#month').empty();
            var $month = $('#month');
        	var $calendar = $('#calendar');
            var month = $calendar.fullCalendar('getDate');
            var initial = month.format('YYYY-MM');
            month.add(-12, 'month');
            for (var i = 0; i < 18; i++) {
                var opt = document.createElement('option');
                opt.value = month.format('YYYY-MM-01');
                opt.text = month.format('MMM YYYY');
                opt.selected = initial === month.format('YYYY-MM');
                $month.append(opt);
                month.add(1, 'month');
            }
        },
	});

	var ApplicationRouter = Backbone.Router.extend({
		// forms = {},
		//map url routes to contained methods
		routes: {
			"": "home",
			"enquiry": "enquiry",
			"suggession": "suggession",
			"complain": "complain",
			"assignment": "assignment",
			"profile": "profile",
			"result": "result",
			"foodcal": "foodcal",
			"curriculum": "curriculum",
			"homework": "homework",
			"weeklyreport": "weeklyreport",
			"edutimetable": "edutimetable",
			"attendance": "attendance",
			"circular": "circular",
			"weeklytimetable": "weeklytimetable",

			"videos": "videos",
			"photos": "photos",
			"event": "event",
			"holidays": "holidays",
			"schoolprofile": "schoolprofile",
			"admission": "admission",
			"achievements": "achievements",
		},
		home: function(){
			var form = new FormView({'model':'home', 'template':'home_form','title':'Home'});
    	   // $('.ti-arrow-circle-left').click();
		},
        enquiry: function(e){

    	   // this.form['enquiry'] = new FormView({'model':'enquiry', 'template':'enquiry_form','title':'Enquiry'});    	   
    	   var form = new FormView({'model':'enquiry', 'template':'enquiry_form','title':'Enquiry'});
    	   $('.ti-arrow-circle-left').click();
        },
        suggession: function(e){
    	   var form = new FormView({'model':'suggession', 'template':'suggession_form','title':'Suggession'});
    	   $('.ti-arrow-circle-left').click();
        },
        complain: function(e){
    	   var form = new FormView({'model':'complain', 'template':'complain_form','title':'Complain'});
    	   $('.ti-arrow-circle-left').click();
        },
        assignment: function(e){
    	   var form = new FormView({'model':'assignment', 'template':'assignment_form','title':'Assignment'});
    	   $('.ti-arrow-circle-left').click();
        },
        profile: function(e){
    		$.get("/getprofile",function(a,b){
				console.log("a>>>>>>>>>",a);
    	   		var form = new FormView({'model':'profile', 'template':'profile_form','title':'Profile','data':a});
    	    });
    	   $('.ti-arrow-circle-left').click();
        },
        result: function(e){
        	$.get("/getresult",'weeklyreport',function(a,b){
  				console.log("a>>>>>>>>>",a);
    	    	var form = new FormView({'model':'result', 'template':'result_form','title':'Result','data':a});
            });
            $('.ti-arrow-circle-left').click();
        },
        foodcal: function(e){
           var form = new FormView({'model':'foodcal', 'template':'foodcal_form','title':'Food Calender'});
           var $calendar = $('#calendar');
			$calendar.fullCalendar('gotoDate', $('#month').val());
           $('.ti-arrow-circle-left').click();
        },
        curriculum: function(e){
    	   var form = new FormView({'model':'curriculum', 'template':'curriculum_form','title':'Curriculum Syllabus & Paper'});
           $('.ti-arrow-circle-left').click();
        },
        homework: function(e){
    	   var form = new FormView({'model':'homework', 'template':'homework_form','title':'Homework'});
 		   $('.ti-arrow-circle-left').click();
        },
        schoolprofile: function(e){
    	   var form = new FormView({'model':'schoolprofile', 'template':'schoolprofile_form','title':'School Profile'});
 		   // $('.ti-arrow-circle-left').click();
        },
        weeklyreport: function(e){
           $.get("/getweeklyreport",'weeklyreport',function(a,b){
  				console.log("a>>>>>>>>>",a);
    	   		var form = new FormView({'model':'weeklyreport', 'template':'weeklyreport_form','title':'Weekly Test Report','data':a});
  			});
    	   $('.ti-arrow-circle-left').click();
        },
        edutimetable: function(e){
    	   var form = new FormView({'model':'edutimetable', 'template':'edutimetable_form','title':'Education Time Table'});
    	   $('.ti-arrow-circle-left').click();
        },
        attendance: function(e){
    	   var form = new FormView({'model':'attendance', 'template':'attendance_form','title':'Attendance'});
    	   $('.ti-arrow-circle-left').click();
        },
        circular: function(e){
           $.get("/getcircular",'weeklyreport',function(a,b){
  				// console.log("a>>>>>>>>>",a);
    	   		var form = new FormView({'model':'circular', 'template':'circular_form','title':'Circular','data':a});
    	   	});
    	   $('.ti-arrow-circle-left').click();
        },
        weeklytimetable: function(e){
    	   var form = new FormView({'model':'weeklytimetable', 'template':'weeklytimetable_form','title':'Weekly & Exam Time Table'});
    	   $('.ti-arrow-circle-left').click();
        },
        videos: function(e){
    	   var form = new FormView({'model':'videos', 'template':'videos_form','title':'videos'});
    	   // $('.ti-arrow-circle-left').click();
        },
        photos: function(e){
        	$.get("/photos",function(a,b){
        		console.log("a>>>>>>>>>",a);
    	   		var form = new FormView({'model':'photos', 'template':'photos_form','title':'Photos','data':a});
    	   		// $('.ti-arrow-circle-left').click();
    	   });
    	   // $('.ti-arrow-circle-left').click();
        },
        event: function(e){
        	$.get("/events",function(a,b){
        		console.log("a>>>>>>>>>",a);
    	   		var form = new FormView({'model':'event', 'template':'event_form','title':'Events','data':a});
        	});
        },
        holidays: function(e){
    	   var form = new FormView({'model':'holidays', 'template':'holidays_form','title':'Holidays'});
        },
        admission: function(e){
    	   var form = new FormView({'model':'admission', 'template':'admission_form','title':'Admission'});
        },
        achievements: function(e){
        	$.get("/achievements",function(a,b){
        		console.log("a>>>>>>>>>",a);
    	   		var form = new FormView({'model':'achievements', 'template':'achievements_form','title':'Achievements','data':a});
        	});
        },

	});

	var ApplicationView = Backbone.View.extend({

		el: $('#sidebar-menu'),
		//observe navigation click events and map to contained methods
		events: {
			'click #enquiry': 'displayEnquiry',
			'click #suggession': 'displaySuggession',
			'click #complain': 'displayComplain',
			'click #assignment': 'dislayAssignment',
			'click #profile': 'dislayProfile',
			'click #result': 'dislayResult',
			'click #foodcal': 'dislayFoodcal',
			'click #curriculum': 'dislayCurriculum',
			'click #homework': 'dislayHomework',			
			'click #weeklyreport': 'dislayWeeklyreport',
			'click #edutimetable': 'dislayEdutimetable',
			'click #attendance': 'dislayAttendance',
			'click #circular': 'dislayCircular',
			'click #weeklytimetable': 'dislayWeeklytimetable',

			'click #videos': 'dislayVideos',
			'click #photos': 'dislayPhotos',
			'click #event': 'dislayEvent',
			'click #holidays': 'dislayHolidays',
			'click #schoolprofile': 'dislaySchoolProfile',
			'click #admission': 'dislayAdmission',						
			'click #achievements': 'dislayAchievements',						
		},

		//called on instantiation
		initialize: function(){
			//set dependency on ApplicationRouter
			this.router = new ApplicationRouter();

			//call to begin monitoring uri and route changes
			Backbone.history.start();
		},

		displayEnquiry: function(){
			this.router.navigate("enquiry", true);	
		},
		displaySuggession: function(){
			this.router.navigate("suggession", true);		
		},
		displayComplain: function(){
			this.router.navigate("complain", true);		
		},
		dislayAssignment: function(){
			this.router.navigate("assignment", true);		
		},
		dislayProfile: function(){
			this.router.navigate("profile", true);		
		},
		dislayResult: function(){
			this.router.navigate("result", true);		
		},
		dislayFoodcal: function(){
			this.router.navigate("foodcal", true);		
		},
		dislayCurriculum: function(){
			this.router.navigate("curriculum", true);		
		},
		dislayEdutimetable: function(){
			this.router.navigate("edutimetable", true);		
		},
		dislayHomework: function(){
			this.router.navigate("homework", true);		
		},
		dislayWeeklyreport: function(){
			this.router.navigate("weeklyreport", true);		
		},
		dislayWeeklytimetable: function(){
			this.router.navigate("weeklytimetable", true);		
		},
		dislayAttendance: function(){
			this.router.navigate("attendance", true);		
		},
		dislayCircular: function(){
			this.router.navigate("circular", true);		
		},
		dislayVideos: function(){
			this.router.navigate("videos", true);		
		},
		dislayPhotos: function(){
			this.router.navigate("photos", true);		
		},
		dislayEvent: function(){
			this.router.navigate("event", true);		
		},
		dislayHolidays: function(){
			this.router.navigate("holidays", true);		
		},
		dislaySchoolProfile: function(){
			this.router.navigate("schoolprofile", true);		
		},
		dislayAdmission: function(){
			this.router.navigate("admission", true);		
		},
		dislayAchievements: function(){
			this.router.navigate("achievements", true);		
		},
	});

	//load application
	new ApplicationView();


});

